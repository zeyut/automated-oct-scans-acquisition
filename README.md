This repository contains files to control with Python the acquisition of OCT scans with a Thorlabs OCT device. This is done with a Python/C API extension for the C++ ThorImage SDK (OctControl.cpp). A file precompiled using ThorImage OCT 4.4.8, Python 3.6 and Microsoft Visual Studio 2017 is also provided (OctControl.pyd). ThorImage SDK is required. 


The functions encoded in OctControl.pyd can be used in combination with a GRBL controlled CNC device (see https://gitlab.com/FlumeAutomation/GRBL_Server.git) in order to automatically synchronize precise positioning of the OCT probe and OCT scans acquisition. This also allows to acquire OCT scans in a mosaic pattern or to monitor the same location over extended periods of time.


Libraries for OCT scans acquisition in combination with the positioning of the probe (eg mosaic pattern) (ImagesAcquisition.py), correction of spherical aberrations (OctCorrection.py) and height map calculation/stitching (ImageProcessing.py) are provided. 

Two worked examples in the form of Jupyter notebooks, depict a sample workflow for images acquisition, and images processing starting from a set of sample scans of a biofilm.

Installation

- install the OCT device (this was tested with a Thorlabs Ganymede II) and ThorImage 4.4.8 on a Windows 7 computer as described by the manufacturer

- download the source code 
  
- install Python 3.6 (https://www.python.org/downloads/) and Visual Studio 2017 redistributable package (https://visualstudio.microsoft.com/downloads/)
  
- open command prompt in the unzipped folder
  
- install the some required Python packages
  ```console
  pip3 install -r requirements.txt
  ```
- if you wish to use automatic OCT scans acquisition in combination with a CNC device follow the instructions in https://gitlab.com/FlumeAutomation/GRBL_Server.git for the setup of the GRBL server, and mount the OCT scan probe on the positioning device


Testing the system using the provided jupyter notebooks

- open command prompt in the unzipped folder

- run Jupyter
  ```console
  jupyter notebook
  ```
- to automatically acquire OCT scans open ImagesAcquisition.ipynb and run the notebook cell by cell by hitting Ctrl-Enter / Shift-Enter (see notes)
- to process the acquired scans open and run ImagesProcessing.ipynb following the comments


